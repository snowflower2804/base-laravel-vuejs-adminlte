<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class AppController extends BaseController
{
    protected $app_api_key;
    protected $domain;
    protected $app_secret_key;
    public function __construct() {
        $this->app_api_key = env('SHOPIFY_APP_API_KEY');
        $this->app_secret_key = env('SHOPIFY_APP_SECRET_KEY');
        $this->domain = env('DOMAIN');

    }

    public function install(Request $request)
    {
        $shop = $request->shop;
        $app_api_key = $this->app_api_key;
        $domain = $this->domain;
        $scopes = 'read_products,write_products,read_customers,write_customers,read_orders,write_orders,read_fulfillments,write_fulfillments,read_locations,read_shipping,write_shipping';
        $install_url = "http://$shop/admin/oauth/authorize?client_id=$app_api_key&scope=$scopes&redirect_uri=https://$domain/app/authenticate-app";
        return redirect($install_url);
    }

    public function authenticate_app(Request $request)
    {
        $shop = $request->shop;
        $code = $request->code;
        $hmac = $request->hmac;
        $app_api_key = $this->app_api_key;
        $domain = $this->domain;
        $app_secret_key = $this->app_secret_key;
        $access_token_url = "https://$shop/admin/oauth/access_token";
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', $access_token_url, [
            'form_params' => [
                'client_id' => $app_api_key,
                'client_secret' => $app_secret_key,
                'code' => $code
            ]
        ]);
        $status = $res->getStatusCode();
        if ($status == 200) {
            $access_token = json_decode($res->getBody())->access_token;
        }
        dd($access_token);
    }
}
