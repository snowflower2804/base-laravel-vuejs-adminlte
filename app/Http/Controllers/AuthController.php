<?php


namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller;
use App\Repositories\UserRepository;
use App\Validators\UserValidator;
use Illuminate\Http\Request;
use \Prettus\Validator\Exceptions\ValidatorException;
use \Prettus\Validator\Contracts\ValidatorInterface;
use Illuminate\Support\Facades\Input;

class AuthController extends Controller
{
    protected $user_repository;
    protected $user_validator;
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct(UserRepository $user_repository, UserValidator $user_validator)
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
        $this->user_reponsitory = $user_repository;
        $this->user_validator = $user_validator;
    }

    public function register(Request $request)
    {
        try {
            $validator = $this->user_validator->with(Input::all())->passesOrFail(ValidatorInterface::RULE_CREATE);
        } catch (ValidatorException $e) {
            return response()->json([
                'error' => true,
                'messages' => $e->getMessageBag()
            ], 200);
        }
        $email = $request['email'];
        $username = $request['username'];
        $password = $request['password'];
        $user = $this->user_reponsitory->findWhere(['username' => strtolower($username)])->first();
        if (!empty($user)) {
            return response()->json(['message' => 'Email exsits']);
        }
        $password = \Hash::make($password);
        $user = $this->user_reponsitory->create([
            'username' => $username,
            'email' => $email,
            'password' => $password
        ]);
        $token = auth()->login($user);
        return response()->json([
            'user' => auth()->user(),
            'access_token' => $token
        ], 200);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['username', 'password']);
        if (!$token = auth()->attempt($credentials)) {
            return response()->json([
                'error' => true,
                'message' => 'Unauthorized'
            ], 200);
        }
        return response()->json([
            'user' => auth()->user(),
            'access_token' => $token
        ], 200);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
