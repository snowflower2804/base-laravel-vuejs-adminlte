<table>
    <thead>
        <tr>
            <th>ORDER_ID</th>
            <th>ORDER_NUMBER</th>
            <th>TRACKING_ID</th>
            <th>TRACKING_URL</th>
            <th>PRODUCTS</th>
            <th>CUSTOMERS</th>
            <th>SHIPPING ADDRESS</th>
            <th>CITY</th>
            <th>PROVINCE</th>
            <th>COUNTRY</th>
            <th>ZIPCODE</th>
            <th>PHONE</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($orders as $order)
            <?php
                $address = '';
                $city = '';
                $province = '';
                $country = '';
                $zipcode = '';
                $phone = '';
                if (isset($order->shipping_address['address1']))
                    $address = $order->shipping_address['address1'];
                if (isset($order->shipping_address['city']))
                    $city = $order->shipping_address['city'];
                if (isset($order->shipping_address['province']))
                    $province = $order->shipping_address['province'];
                if (isset($order->shipping_address['country']))
                    $country = $order->shipping_address['country'];
                if (isset($order->shipping_address['zip']))
                    $zipcode = $order->shipping_address['zip'];
                if (isset($order->shipping_address['phone']))
                    $phone = $order->shipping_address['phone'];
            ?>
            <tr>
                <td>{{ $order->id }}</td>
                <td>{{ $order->name }}</td>
                <td></td>
                <td></td>
                <td>
                    <ul>
                        @foreach ($order->line_items as $item)
                            <li>
                                <label>{{ $item['title'] }} - {{ isset($item['variant_title']) ? $item['variant_title'] : 'Default Title' }} - {{ $item['quantity'] }}</label>
                            </li>
                        @endforeach
                    </ul>
                </td>
                <td>{{ isset($order->customer->first_name) ? $order->customer->first_name : '' }} {{ isset($order->customer->last_name) ? $order->customer->last_name : ''}}</td>
                <td>{{ $address }}</td>
                <td>{{ $city }}</td>
                <td>{{ $province }}</td>
                <td>{{ $country }}</td>
                <td>{{ $zipcode }}</td>
                <td>{{ $phone }}</td>
            </tr>
        @endforeach
    </tbody>
</table>