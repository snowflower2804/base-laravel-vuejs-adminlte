import Shopify_Store_Index from '../components/page/ShopifyStore/index'
import Login from '../components/page/Auth/login'
import Signup from '../components/page/Auth/signup'
import Dashboard from '../components/Dashboard.vue'
import DashboardIndex from '../components/modules/dashboard/Index.vue'
import NotFound from '../components/modules/dashboard/404.vue'
// import WidgetsIndex from '../components/modules/widgets/Index.vue'
// import UIButtons from '../components/modules/ui/Buttons.vue'
// import UIGeneral from '../components/modules/ui/General.vue'
// import UIIcons from '../components/modules/ui/Icons.vue'
// import UIModals from '../components/modules/ui/Modals.vue'
// import UISliders from '../components/modules/ui/Sliders.vue'
// import UITimeline from '../components/modules/ui/Timeline.vue'

// Routes
const routes = [{
    path: '/login',
    name: 'login',
    component: Login,
    beforeEnter: (to, from, next) => {
        document.body.className = 'login-page'
        next()
    }
},{
    path: '/register',
    name: 'register',
    component: Signup,
    beforeEnter: (to, from, next) => {
        document.body.className = 'login-page'
        next()
    }
}, {
    path: '/',
    component: Dashboard,
    meta: {requiresAuth: true},
    beforeEnter: (to, from, next) => {
        document.body.className = ' skin-blue sidebar-mini'
        next()
    },
    children: [{
        path: '',
        redirect: '/dashboard'
    }, {
        path: '/dashboard',
        name: 'dashboard',
        component: DashboardIndex,
        meta: {requiresAuth: true},
    }, {
        path: '/install',
        component: Shopify_Store_Index,
        meta: { requiresAuth: true }
    }, {
        path: '*',
        name: '404',
        component: NotFound,
        meta: {requiresAuth: true},
    }]
}, {
    // not found handler
    path: '*',
    redirect: '/login'
}]

export default routes