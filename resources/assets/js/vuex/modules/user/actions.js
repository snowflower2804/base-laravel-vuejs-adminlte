import { post, get, queryString } from '../../http/http';

const login = function ({commit}, credentials) {
    return new Promise((resolve, reject) => {
        try {
            post('api/login', credentials).then(response => {
                let data = response.data;
                let logged_in = false;
                if (data.access_token != undefined) {
                    window.localStorage.setItem("access_token", data.access_token);
                    commit('LOGIN_SUCCESS', data.user);
                    logged_in = true;
                }
                resolve({logged_in: logged_in, data: data});
            });
        } catch (ex) {
            reject(ex);
        }
    });
}

const signup = function({commit}, credentials) {
    return new Promise((resolve, reject) => {
        try {
            post('api/register', credentials).then(response => {
                let data = response.data;
                let logged_in = false;
                if (data.access_token != undefined) {
                    window.localStorage.setItem("access_token", data.access_token);
                    logged_in = true;
                    commit('LOGIN_SUCCESS', data.user);
                }
                resolve({logged_in: logged_in, data: data});
            });
        } catch (ex) {
            reject(ex);
        }
    })
}

export default {
    login,
    signup
};