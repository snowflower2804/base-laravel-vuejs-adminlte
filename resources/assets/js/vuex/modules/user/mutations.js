const LOGIN_SUCCESS = (state, user) => {
    window.localStorage.setItem("user", JSON.stringify(user));
};
export default {
    LOGIN_SUCCESS
};